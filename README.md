# Nginx Redirection

Demonstrates how to use the [`nginx` S2I image](https://github.com/sclorg/nginx-container) to set up redirections.

## How to use

- [Fork this project](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork)
- Modify [`nginx-default-cfg/redirect.conf`](nginx-default-cfg/redirect.conf) as needed
- Deploy in an [OKD4 PaaS project](https://paas.docs.cern.ch):

```bash
# Replace GitLab URL with the fork
oc new-app nginx~https://gitlab.cern.ch/paas-tools/paas-examples/nginx-redirection.git --name=nginx-redirection
# Be nice with resource quota - nginx doesn't need much resources
oc set resources deployment/nginx-redirection --limits=cpu=1,memory=150Mi --requests=cpu=20m,memory=30Mi
# Serve the nginx-redirection service at old-location.web.cern.ch 
oc create route edge nginx-redirection --hostname=old-location.web.cern.ch --service=nginx-redirection
# Expose nginx-redirection to Internet
oc annotate route nginx-redirection haproxy.router.openshift.io/ip_whitelist='' --overwrite
```

## References

- `nginx` S2I image: <https://github.com/sclorg/nginx-container>
- Surrounding nginx configuration including our config files: <https://github.com/sclorg/nginx-container/blob/7e23092/examples/1.20/test-app/nginx.conf#L48>
- Use of `location` blocks: <https://docs.nginx.com/nginx/admin-guide/web-server/web-server/#locations>
- Redirection/rewrite rules: <https://www.nginx.com/blog/creating-nginx-rewrite-rules/>

